import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { List, ListItem, ListItemText, Paper } from '@mui/material';
import { Link } from 'react-router-dom';

function TaskList() {
    const [tasks, setTasks] = useState([]);
    const [error, setError] = useState('');

    useEffect(() => {
        axios.get('http://localhost:8080/tasks')
            .then(response => {
                setTasks(response.data.tasks); 
            })
            .catch(error => {
                console.error('Error fetching tasks:', error);
                setError('Failed to fetch tasks.');
            });
    }, []);

    if (error) {
        return <p>{error}</p>;
    }

    return (
        <Paper style={{ margin: 16, padding: 16 }}>
            <List>
                {tasks.length > 0 ? tasks.map(task => (
                    <ListItem key={task.id} button component={Link} to={`/edit-task/${task.id}`}>
                        <ListItemText primary={task.description} />
                    </ListItem>
                )) : <p>No tasks found.</p>}
            </List>
        </Paper>
    );
}

export default TaskList;

import React, { useState } from 'react';
import { TextField, Button, Paper } from '@mui/material';
import axios from 'axios';

function AddTask() {
  const [description, setDescription] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    axios.post('http://localhost:8080/tasks/create', { description })
      .then(response => {
      })
      .catch(error => console.error('Error adding task:', error));
  };

  return (
    <Paper style={{ margin: 16, padding: 16 }}>
      <form onSubmit={handleSubmit}>
        <TextField
          label="Task Description"
          value={description}
          onChange={e => setDescription(e.target.value)}
          margin="normal"
          fullWidth
        />
        <Button type="submit" color="primary" variant="contained">
          Add Task
        </Button>
      </form>
    </Paper>
  );
}

export default AddTask;

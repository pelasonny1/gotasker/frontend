import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { TextField, Button, Paper } from '@mui/material';
import axios from 'axios';

function EditTask() {
  const { id } = useParams();
  const [description, setDescription] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    axios.get(`http://localhost:8080/tasks/${id}`)
      .then(response => {
        setDescription(response.data.description);
      })
      .catch(error => console.error('Error fetching task:', error));
  }, [id]);

  const handleSubmit = (event) => {
    event.preventDefault();
    axios.put(`http://localhost:8080/tasks/update/${id}`, { description })
      .then(() => {
        navigate('/');
      })
      .catch(error => console.error('Error updating task:', error));
  };

  return (
    <Paper style={{ margin: 16, padding: 16 }}>
      <form onSubmit={handleSubmit}>
        <TextField
          label="Task Description"
          value={description}
          onChange={e => setDescription(e.target.value)}
          margin="normal"
          fullWidth
        />
        <Button type="submit" color="primary" variant="contained">
          Update Task
        </Button>
      </form>
    </Paper>
  );
}

export default EditTask;
